## How to run:

### From the program directory run ###
    
    donet run

    Because we are not passing parameters above the program will use default parameters value setup in 
    appsettings.Development.json or appsettings.Production.json

### To override default parameters, see examples below (notice that omitted parameters will come from default parameters) ###

    dotnet run initialDate="2020-08-01" year=2020 deliveryCode="BR06" leadDays=4

    or

    dotnet run initialDate="2020-08-01" year=2020 deliveryCode="BR06"

    or

    dotnet run initialDate="2020-08-01" year=2020

    or

    any combination

    dotnet deliveryCode="BR06"